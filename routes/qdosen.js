var express = require('express');
var router = express.Router();
var db = require('./db');

router.get('/:kode_kuliah', function(req,res,next){
  //front end login (ada di folder views)
  var matkul = req.params.kode_kuliah;
  let query = "SELECT * FROM questions WHERE question_id LIKE 'D%'";

  db.query(query, (err, result) => {
      if (err) {
          res.redirect('/');
      }
      /*
      res.send({
        questions: result,
      })
      */
      res.render('qdosen', {
          title: "Kuesioner Evaluasi Dosen",
          kode_kuliah: matkul,
          questions: result
      });
      //console.log(result.length);
      //console.log(result);
  });
});

router.post('/:kode_kuliah/:question_id', function(req, res){
  var response = {
    "response_id": req.body.response_id,
    "survey_id": req.params.kode_kuliah,
    "question_id": req.params.question_id,
    "response": req.body.response
  }

  db.query("INSERT INTO responses SET ?", response, (err, results, fields) => {
    if (err){
      console.log("error ocurred",error);
      res.send({
        "code":400,
        "failed":"error occurred"
      });
    }else{
      console.log('berhasil menyimpan response');
      res.send({
        "code": 200,
        "status": "saved"
      });
    }
  });
});

module.exports = router;
